<?php
/**
 * @name StudentModel
 * @author ruansheng
 * @desc 学生
 */
class StudentModel {

    private $masterCollection = null;

    public function __construct() {
        $this->masterCollection = MongoClientFactory::getExamMongoClient()->selectCollection('student');
    }

    /**
     * 获取学生列表
     * @param array $query
     * @param array $fields
     * @param array $sort
     * @param int $index
     * @param int $limit
     * @return array
     */
    public function getStudentList($query = [], $fields = [], $sort = [], $index = 0, $limit = 10){
        $rows = $this->masterCollection->find($query)->fields($fields)->sort($sort)->skip($index)->limit($limit);
        $list = [];
        foreach($rows as $row){
            $list[] = $row;
        }

        return $list;
    }

    /**
     * 查询学生数
     * @param array $query
     * @return int
     */
    public function getStudentCount($query = []) {
        $count = $this->masterCollection->find($query)->count();
        return $count;
    }

    /**
     * 添加学生
     * @param $number
     * @param $username
     * @param $password
     * @param $sex
     * @return bool
     */
    public function addStudent($number, $username, $sex, $password) {
        $query = array(
            'number' => $number
        );
        $row = $this->masterCollection->findOne($query);
        if($row) {
            return array(false, '该学生已存在');
        }
        $data = array(
            'number' => $number,
            'name' => $username,
            'password' => sha1($password),
            'time' => time(),
            'status' => 0,
            'sex' => $sex,
        );
        $item = $this->masterCollection->insert($data, array('safe'=>true));
        if($item['ok'] != 1) {
            return array(false, '添加失败');
        }
        return array($data['_id']->{'$id'}, '添加成功');
    }

    /**
     * 登录检测
     * @param $number
     * @param $password
     * @return bool
     */
    public function login($number, $password) {
        $query = array(
            'number' => $number,
            'password' => sha1($password),
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '学号或密码错误');
        }
        return array($row['_id']->{'$id'}, '登录成功');
    }

    /**
     * 获取info
     * @param $_id
     * @return array
     */
    public function getStudentInfo($_id) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $info = $this->masterCollection->findOne($query);
        return $info;
    }

    /**
     * 保存设置
     * @param $_id
     * @param $password
     * @return bool
     */
    public function saveSetting($_id, $password) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '学生不存在');
        }
        $data = array(
            'password' => sha1($password),
        );
        $status = $this->masterCollection->update($query, $data);
        if(!$status) {
            return array(false, '更新失败');
        }
        return array(true, '更新成功');
    }

    /**
     * 删除
     * @param $_id
     * @return array
     */
    public function deleteStudent($_id){
        $query = array(
            '_id' => new MongoId($_id)
        );
        $status = $this->masterCollection->remove($query);
        if(!$status) {
            return array(false, '删除失败');
        }
        return array(true, '删除成功');
    }

    /**
     * 保存学生
     * @param $_id
     * @param $number
     * @param $name
     * @param $sex
     * @param $password
     * @return array
     */
    public function saveStudent($_id, $number, $name, $sex, $password) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '学生不存在');
        }
        $data = array(
            'number' => $number,
            'name' => $name,
           'password' => $password,
            'time' => time(),
            'sex' => $sex,
        );
        $status = $this->masterCollection->update($query,array('$set'=>$data));
        if(!$status) {
            return array(false, '更新失败');
        }
        return array(true, '更新成功');
    }

}
