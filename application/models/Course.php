<?php
/**
 * @name StudentModel
 * @author ruansheng
 * @desc 课程
 */
class CourseModel {

    private $masterCollection = null;

    public function __construct() {
        $this->masterCollection = MongoClientFactory::getExamMongoClient()->selectCollection('course');
    }

    /**
     * 添加课程
     * @param $name
     * @return bool
     */
    public function addCourse($name) {
        $query = array(
            'name' => $name
        );
        $row = $this->masterCollection->findOne($query);
        if($row) {
            return array(false, '该课程已存在');
        }
        $data = array(
			'name' => $name,
            'time' => time(),
            'update_time' => time(),
            'status' => 0,
        );
        $item = $this->masterCollection->insert($data, array('safe'=>true));
        if($item['ok'] != 1) {
            return array(false, '添加失败');
        }
        return array($data['_id']->{'$id'}, '添加成功');
    }

    /**
     * 查询班级数
     * @param array $query
     * @return int
     */
    public function getCourseCount($query = []) {
        $count = $this->masterCollection->find($query)->count();
        return $count;
    }

    /**
     * 获取班级list
     * @param array $query
     * @param array $fields
     * @param array $sort
     * @param int   $index
     * @param int   $limit
     * @return mixed
     */
    public function getCourseList($query = [], $fields = [], $sort = [], $index = 0, $limit = 10) {
        $rows = $this->masterCollection->find($query)->fields($fields)->sort($sort)->skip($index)->limit($limit);
        $list = [];
        foreach($rows as $row) {
            $row['_id'] = $row['_id']->__toString();
            $list[] = $row;
        }

        return $list;
    }

    /**
     * 获取info
     * @param $_id
     * @return array
     */
    public function getCourseInfo($_id) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $info = $this->masterCollection->findOne($query);
        return $info;
    }

    /**
     * 保存班级
     * @param $_id
     * @param $name
     * @return bool
     */
    public function saveCourse($_id, $name) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '课程不存在');
        }
        $data = array(
            'name' => $name,
            'update_time' => time(),
        );
        $status = $this->masterCollection->update($query,array('$set'=>$data));
        if(!$status) {
            return array(false, '更新失败');
        }
        return array(true, '更新成功');
    }

    /**
     * 删除
     * @param $_id
     * @return array
     */
    public function deleteCourse($_id){
        $query = array(
            '_id' => new MongoId($_id)
        );
        $status = $this->masterCollection->remove($query);
        if(!$status) {
            return array(false, '删除失败');
        }
        return array(true, '删除成功');
    }
}
