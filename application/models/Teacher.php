<?php
/**
 * @name TeacherModel
 * @author ruansheng
 * @desc 教师
 */
class TeacherModel {

    private $masterCollection = null;

    public function __construct() {
        $this->masterCollection = MongoClientFactory::getExamMongoClient()->selectCollection('teacher');
    }

    /**
     * 添加老师
     * @param $number
     * @param $username
     * @param $class
     * @param $course
     * @param $password
     * @return array
     */
    public function addTeacher($number, $username, $class, $course, $password) {
        $query = array(
            'number' => $number
        );
        $row = $this->masterCollection->findOne($query);
        if($row) {
            return array(false, '该教师已存在');
        }
        $data = array(
            'number' => $number,
            'name' => $username,
            'class' => $class,
            'course' => $course,
            'password' => sha1($password),
            'time' => time(),
            'status' => 0,
        );
        $item = $this->masterCollection->insert($data, array('safe'=>true));
        if($item['ok'] != 1) {
            return array(false, '添加失败');
        }
        return array($data['_id']->{'$id'}, '添加成功');
    }

    /**
     * 教师登录检测
     * @param $number
     * @param $password
     * @return bool
     */
    public function login($number, $password) {
        $query = array(
            'number' => $number,
            'password' => sha1($password),
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '账号或密码错误');
        }
        return array($row['_id']->{'$id'}, '登录成功');
    }

    /**
     * 获取info
     * @param $_id
     * @return array
     */
    public function getTeacherInfo($_id) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $info = $this->masterCollection->findOne($query);
        return $info;
    }

    /**
     * 保存设置
     * @param $_id
     * @param $password
     * @return bool
     */
    public function saveSetting($_id, $password) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '账号不存在');
        }
        $data = array(
            'password' => sha1($password),
        );
        $status = $this->masterCollection->update($query, $data);
        if(!$status) {
            return array(false, '更新失败');
        }
        return array(true, '更新成功');
    }

    /**
     * 查询老师数
     * @param array $query
     * @return int
     */
    public function getTeacherCount($query = []) {
        $count = $this->masterCollection->find($query)->count();
        return $count;
    }

    /**
     * 获取老师list
     * @param array $query
     * @param array $fields
     * @param array $sort
     * @param int   $index
     * @param int   $limit
     * @return mixed
     */
    public function getTeacherList($query = [], $fields = [], $sort = [], $index = 0, $limit = 10) {
        $rows = $this->masterCollection->find($query)->fields($fields)->sort($sort)->skip($index)->limit($limit);
        $list = [];
        foreach($rows as $row) {
            $row['_id'] = $row['_id']->__toString();
            $list[] = $row;
        }

        return $list;
    }

    /**
     * 保存教师信息
     * @param $_id
     * @param $name
     * @param $number
     * @param $class
     * @param $course
     * @param $password
     * @return array
     */
    public function saveTeacher($_id, $name, $number, $class, $course, $password = null) {
        $query = array(
            '_id' => new MongoId($_id)
        );
        $row = $this->masterCollection->findOne($query);
        if(empty($row)) {
            return array(false, '教师不存在');
        }
        $data = array(
            'name' => $name,
            'number' => $number,
            'class' => $class,
            'course' => $course,
            'update_time' => time(),
        );

        if($password != null){
            $data['password'] = sha1($password);
        }
        $status = $this->masterCollection->update($query,array('$set'=>$data));
        if(!$status) {
            return array(false, '更新失败');
        }
        return array(true, '更新成功');
    }

    /**
     * 删除
     * @param $_id
     * @return array
     */
    public function deleteTeacher($_id){
        $query = array(
            '_id' => new MongoId($_id)
        );
        $status = $this->masterCollection->remove($query);
        if(!$status) {
            return array(false, '删除失败');
        }
        return array(true, '删除成功');
    }
}
