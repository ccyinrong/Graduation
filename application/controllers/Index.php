<?php

/**
 * @name IndexController
 * @author ruansheng
 */
class IndexController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active', 1);
    }

    /**
     * 首页
     * http://115.28.74.55:9999/index/index
     * @return bool
     */
	public function indexAction() {
        return true;
	}

    /**
     * test
     * http://115.28.74.55:9999/index/test
     * @return bool
     */
    public function testAction() {
        return true;
    }
}
