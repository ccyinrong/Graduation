<?php

/**
 * @name CourseController
 * @author ruansheng
 * @desc 课程
 */
class CourseController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active', 4);
    }

    /**
     * http://115.28.74.55:9999/course/index
     * @return bool
     */
	public function indexAction() {
        $p = $this->getRequest()->getRequest('p',1);

        $query = [];
        $fields = [];
        $sort = ['time' => -1];
        $index = 0;
        $limit = 100;

        $Course = new CourseModel();

        // 查询班级数
        $count = $Course->getCourseCount($query);

        // 查询班级列表
        $rows = $Course->getCourseList($query, $fields, $sort, $index, $limit);
		
        $list = [];
        foreach($rows as $v) {
            $v['date'] = date('Y-m-d H:i:s', $v['time']);
            if($v['update_time'] == 0) {
                $v['update_date'] = '--';
            } else {
                $v['update_date'] = date('Y-m-d H:i:s', $v['update_time']);
            }
            $list[] = $v;
        }

        // 计算分页
        $pager = PagerLib::getPager($count, $limit, $p);

        $pagers = $pager['pagers'];

        $this->getView()->assign('list', $list);
        $this->getView()->assign('pagers', $pagers);
        return true;
	}
	/**
     * 添加
     * http://115.28.74.55:9999/course/add
     * @return bool
     */
    public function addAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        if(empty($data['course_name'])) {
            $this->responseJson(401, '课程名称不能为空');
        }

        $Course = new CourseModel();
        $ret = $Course->addCourse($data['course_name']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }
	
	/**
     * 修改
     * http://115.28.74.55:9999/course/save
     * @return bool
     */
    public function saveAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        if(empty($data['_id'])) {
            $this->responseJson(401, '课程编号不能为空');
        }

        if(empty($data['course_name'])) {
            $this->responseJson(401, '课程名称不能为空');
        }

        $Course = new CourseModel();
        $ret = $Course->saveCourse($data['_id'], $data['course_name']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }
	
	/**
     * 删除
     * http://115.28.74.55:9999/course/delete
     * @return bool
     */
    public function deleteAction() {
        $data = $this->getRequest()->getRequest();

        if(!isset($data['_id'])) {
            $this->responseJson(401, '_id no isset');
        } else if(empty($data['_id'])){
            $this->responseJson(401, '_id is empty');
        }

        $Course = new CourseModel();
        $ret = $Course->deleteCourse($data['_id']);

        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(402, $ret[1]);
        }

        return false;
    }
}
