<?php

/**
 * @name LoginController
 * @author ruansheng
 * @desc 登录
 */
class LoginController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('layer_nav_second', '个人中心');
    }

    /**
     * 登录
     * http://115.28.74.55:9999/login/index
     */
    public function indexAction() {
        return true;
    }

    /**
     * 登录
     * http://115.28.74.55:9999/login/execLogin
     * @return bool
     */
    public function execLoginAction() {
        $data = $this->getRequest()->getRequest();

        if(!isset($data['role'])) {
            $this->responseJson(401, 'role is empty');
        } else if(empty($data['role'])){
            $this->responseJson(401, 'role is empty');
        }

        if(!isset($data['number'])) {
            $this->responseJson(401, 'number is empty');
        } else if(empty($data['number'])){
            $this->responseJson(401, 'number is empty');
        }

        if(!isset($data['password'])) {
            $this->responseJson(401, 'password is empty');
        } else if(empty($data['password'])){
            $this->responseJson(401, 'password is empty');
        }

        $ret = array(false, 'error');

        $role = $data['role'];
        switch($role) {
            case 1: //学生
                $Student = new StudentModel();
                $ret = $Student->login($data['number'], $data['password']);
                break;
            case 2: //教师
                $Teacher = new TeacherModel();
                $ret = $Teacher->login($data['number'], $data['password']);
                break;
            case 3: //管理员
                $Manager = new ManagerModel();
                $ret = $Manager->login($data['number'], $data['password']);
                break;
            default:
                $this->responseJson(402, 'role is empty');
                break;
        }

        if($ret[0]) {
            $_SESSION['role'] = $role;
            $_SESSION['_id'] = $ret[0];
            $this->responseJson(200, '登录成功');
        } else {
            $this->responseJson(402, $ret[1]);
        }
        return false;
    }

    /**
     * 退出
     * http://115.28.74.55:9999/login/index
     * @return bool
     */
    public function logoutAction() {
        unset($_SESSION['_id']);
        header('Location:/login/index');
        exit;
        return false;
    }

    /**
     *  保存
     */
    public function saveSettingAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $password = $_POST['password'];
        if(empty($password)) {
            $this->responseJson(401, '密码不能为空');
        }

        $Student = new StudentModel();
        $ret = $Student->saveSetting($this->_id, $password);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    //----------------
    /**
     * 测试添加学生
     * http://115.28.74.55:9999/login/addStudent
     */
    public function addStudentAction() {
        $Student = new StudentModel();
        $ret = $Student->addStudent('9527', 'zhangsan', 123);
        echo '<pre>';
            print_r($ret);
        return false;
    }

    /**
     * 测试添加教师
     * http://115.28.74.55:9999/login/addTeacher
     */
    public function addTeacherAction() {
        $Teacher = new TeacherModel();
        $ret = $Teacher->addTeacher('9528', 'lisi', 123);
        echo '<pre>';
            print_r($ret);
        return false;
    }

    /**
     * 测试添加管理员
     * http://115.28.74.55:9999/login/addManager
     */
    public function addManagerAction() {
        $Manager = new ManagerModel();
        $ret = $Manager->addManager('9529', 'wangwu', 123);
        echo '<pre>';
            print_r($ret);
        return false;
    }
}
