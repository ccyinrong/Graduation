<?php

/**
 * @name TestController
 * @author ruansheng
 * @desc 试卷管理
 */
class TestController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active',5);
    }

    /**
     *
     * @return bool
     */
    public function indexAction() {
        $query = [];
        $fields = [];
        $sort = ['time' => -1];
        $index = 0;
        $limit = 100;

        // 获取课程列表
        $Course = new CourseModel();
        $courses = $Course->getCourseList();

        $Test = new TestModel();

        // 查询班级数
        $count = $Test->getTestCount($query);

        // 查询班级列表
        $rows = $Test->getTestList($query, $fields, $sort, $index, $limit);

        $list = [];
        foreach($rows as $v) {
            foreach($courses as $j) {
                if($j['_id'] == $v['course_id']) {
                    $v['course_name'] = $j['name'];
                }
            }
            $v['date'] = date('Y-m-d H:i:s', $v['time']);
            if($v['update_time'] == 0) {
                $v['update_date'] = '--';
            } else {
                $v['update_date'] = date('Y-m-d H:i:s', $v['update_time']);
            }
            $list[] = $v;
        }

        $this->getView()->assign('list', $list);
        $this->getView()->assign('courses', $courses);
        return true;
    }

    /**
     * 添加
     * @return bool
     */
    public function addAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        if(empty($data['name'])) {
            $this->responseJson(401, '试卷标题不能为空');
        }

        if(empty($data['course_id'])) {
            $this->responseJson(401, '课程不能为空');
        }

        $Test = new TestModel();
        $ret = $Test->addTest($data['name'], $data['course_id']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    /**
     * 修改
     * http://115.28.74.55:9999/test/save
     * @return bool
     */
    public function saveAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        if(empty($data['_id'])) {
            $this->responseJson(401, '试卷不能为空');
        }

        if(empty($data['name'])) {
            $this->responseJson(401, '试卷标题不能为空');
        }

        if(empty($data['course_id'])) {
            $this->responseJson(401, '课程不能为空');
        }

        $Test = new TestModel();
        $ret = $Test->saveTest($data['_id'], $data['name'], $data['course_id']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    /**
     * 删除
     * @return bool
     */
    public function deleteAction() {
        $data = $this->getRequest()->getRequest();

        if(!isset($data['_id'])) {
            $this->responseJson(401, '_id no isset');
        } else if(empty($data['_id'])){
            $this->responseJson(401, '_id is empty');
        }

        $Test = new TestModel();
        $ret = $Test->deleteTest($data['_id']);

        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(402, $ret[1]);
        }

        return false;
    }

}
