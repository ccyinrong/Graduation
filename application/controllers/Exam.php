<?php

/**
 * @name ExamController
 * @author ruansheng
 * @desc 考试
 */
class ExamController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active', 6);
    }

    /**
     * http://115.28.74.55:9999/exam/index
     * @return bool
     */
	public function indexAction() {
        return true;
	}

    /**
     * http://115.28.74.55:9999/exam/myexam
     * @return bool
     */
    public function myexamAction() {
        return true;
    }
}
