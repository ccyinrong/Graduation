<?php

/**
 * @name ClassController
 * @author ruansheng
 * @desc 班级
 */
class ClassController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active', 8);
    }

    /**
     * http://115.28.74.55:9999/class/index
     * @return bool
     */
	public function indexAction() {
        $p = $this->getRequest()->getRequest('p',1);

        $query = [];
        $fields = [];
        $sort = ['time' => -1];
        $index = 0;
        $limit = 100;

        $Class = new ClassModel();

        // 查询班级数
        $count = $Class->getClassCount($query);

        // 查询班级列表
        $rows = $Class->getClassList($query, $fields, $sort, $index, $limit);

        $list = [];
        foreach($rows as $v) {
            $v['date'] = date('Y-m-d H:i:s', $v['time']);
            if($v['update_time'] == 0) {
                $v['update_date'] = '--';
            } else {
                $v['update_date'] = date('Y-m-d H:i:s', $v['update_time']);
            }
            $list[] = $v;
        }

        // 计算分页
        $pager = PagerLib::getPager($count, $limit, $p);

        $pagers = $pager['pagers'];

        $this->getView()->assign('list', $list);
        $this->getView()->assign('pagers', $pagers);
        return true;
	}

    /**
     * 添加
     * http://115.28.74.55:9999/class/add
     * @return bool
     */
    public function addAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        if(empty($data['class_name'])) {
            $this->responseJson(401, '班级名称不能为空');
        }

        if(count($data['class_count']) == 0) {
            $this->responseJson(401, '班级人数不能为空');
        }

        $Class = new ClassModel();
        $ret = $Class->addClass($data['class_name'], $data['class_count']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    /**
     * 修改
     * http://115.28.74.55:9999/class/save
     * @return bool
     */
    public function saveAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        if(empty($data['_id'])) {
            $this->responseJson(401, '班级不能为空');
        }

        if(empty($data['class_name'])) {
            $this->responseJson(401, '班级名称不能为空');
        }

        if(count($data['class_count']) == 0) {
            $this->responseJson(401, '班级人数不能为空');
        }

        $Class = new ClassModel();
        $ret = $Class->saveClass($data['_id'], $data['class_name'], $data['class_count']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    /**
     * 删除
     * http://115.28.74.55:9999/class/delete
     * @return bool
     */
    public function deleteAction() {
        $data = $this->getRequest()->getRequest();

        if(!isset($data['_id'])) {
            $this->responseJson(401, '_id no isset');
        } else if(empty($data['_id'])){
            $this->responseJson(401, '_id is empty');
        }

        $Class = new ClassModel();
        $ret = $Class->deleteClass($data['_id']);

        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(402, $ret[1]);
        }

        return false;
    }
}
