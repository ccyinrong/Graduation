<?php

/**
 * @name TeacherController
 * @author ruansheng
 * @desc 教师
 */
class TeacherController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active', 3);
    }

    /**
     * http://115.28.74.55:9999/teacher/index
     * @return bool
     */
	public function indexAction() {
        $p = $this->getRequest()->getRequest('p',1);

        $query = [];
        $fields = [];
        $sort = ['time' => -1];
        $index = 0;
        $limit = 100;

        $Teacher = new TeacherModel();

        // 查询教师数
        $count = $Teacher->getTeacherCount($query);

        // 查询教师列表
       $rows = $Teacher->getTeacherList($query, $fields, $sort, $index, $limit);

        $list = [];
        foreach($rows as $v) {
            $list[] = $v;
        }

        //查询班级列表
        $Class = new ClassModel();
        $classList = $Class->getClassList($query, $fields, $sort, $index, $limit);

        //查询课程列表
        $Course = new CourseModel();
        $courseList = $Course->getCourseList($query, $fields, $sort, $index, $limit);

        // 计算分页
        $pager = PagerLib::getPager($count, $limit, $p);

        $pagers = $pager['pagers'];

        $this->getView()->assign('list', $list);
        $this->getView()->assign('classList', $classList);
        $this->getView()->assign('courseList', $courseList);
        $this->getView()->assign('pagers', $pagers);
        return true;
	}

    /**
     * 添加
     * http://115.28.74.55:9999/teacher/add
     * @return bool
     */
    public function addAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        $Teacher = new TeacherModel();
        $ret = $Teacher->addTeacher($data['name'], $data['number'], $data['class'], $data['course'], $data['password']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    /**
     * 修改
     * http://115.28.74.55:9999/teacher/save
     * @return bool
     */
    public function saveAction() {
        if(!$this->getRequest()->isPost()) {
            $this->responseJson(401, '请求方式不正确');
        }

        $data = $this->getRequest()->getRequest();

        $Teacher = new TeacherModel();
        $ret = $Teacher->saveTeacher($data['_id'], $data['name'], $data['number'], $data['class'], $data['course'], $data['password']);
        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(401, $ret[1]);
        }
        return false;
    }

    /**
     * 删除
     * http://115.28.74.55:9999/teacher/delete
     * @return bool
     */
    public function deleteAction() {
        $data = $this->getRequest()->getRequest();

        if(!isset($data['_id'])) {
            $this->responseJson(401, '_id no isset');
        } else if(empty($data['_id'])){
            $this->responseJson(401, '_id is empty');
        }

        $Teacher = new TeacherModel();
        $ret = $Teacher->deleteTeacher($data['_id']);

        if($ret[0]) {
            $this->responseJson(200, $ret[1]);
        } else {
            $this->responseJson(402, $ret[1]);
        }

        return false;
    }

}
