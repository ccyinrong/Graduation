<?php

/**
 * @name SystemController
 * @author ruansheng
 * @desc 系统设置
 */
class SystemController extends Base {

    public function init() {
        parent::init();
        $this->getView()->assign('active', 7);
    }

    /**
     * http://115.28.74.55:9999/system/index
     * @return bool
     */
	public function indexAction() {
        return true;
	}

}
