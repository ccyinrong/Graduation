<?php
date_default_timezone_set('PRC');
define('APPLICATION_PATH', dirname(__FILE__).'/../');

$application = new Yaf_Application( APPLICATION_PATH . "/conf/application.ini");

function overwrite_error_log($errorno, $errorstr, $errorfile, $errorline) {
    $curerrorno = error_reporting();
    if (($curerrorno & ~$errorno) == $curerrorno) {
        return true;
    }
    $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
    $exit = false;
    switch ($errorno) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error_type = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error_type = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error_type = 'Fatal Error';
            $exit = true;
            break;
        default:
            $error_type = 'Fatal Error';
            $exit = true;
            break;
    }
    $timezone = date_default_timezone_get();
    $request_uri_text = $request_uri ? '   [REQUEST_URI:' . $request_uri . ']' : '   [REQUEST_URI: Unkown]';
    $text = '[' . date('Y-m-d H:i:s', time()) . ' ' . $timezone . '] ' . ' PHP' . ' ' . $error_type . ':  ' . $errorstr . ' in ' . $errorfile . ' on line ' . $errorline . $request_uri_text . PHP_EOL.PHP_EOL;

    $log_path = '/var/log/php-fpm.log';
    if (is_writeable($log_path)) {
        file_put_contents($log_path, $text, FILE_APPEND);
    }
    if ($exit) {
        die();
    }
    return true;
}

set_error_handler('overwrite_error_log', E_ALL & ~E_DEPRECATED & ~E_STRICT);


$application->bootstrap()->run();
